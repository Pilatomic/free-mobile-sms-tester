#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    settings = new QSettings("parametre.ini", QSettings::IniFormat,this);

    ui->setupUi(this);

    connect(ui->sendButton,SIGNAL(clicked()),this,SLOT(sendMessage()));

    loadSettings();
}

void MainWindow::sendMessage(){
    QString user = ui->userLineEdit->text();
    QString pass = ui->passLineEdit->text();
    QString message = ui->textEdit->toPlainText();

    QUrlQuery params;
    params.addQueryItem("user",user);
    params.addQueryItem("pass",pass);
    params.addQueryItem("msg",message);

    QUrl url("https://smsapi.free-mobile.fr/sendmsg");
    url.setQuery(params);

    QNetworkRequest request(url);

    networkReply = networkAccessManager.get(request);

    connect(networkReply,SIGNAL(finished()),this,SLOT(finished()));
}

void MainWindow::finished(){
    QMessageBox msgBox(this);
    msgBox.setWindowTitle(windowTitle());

    int httpCode = networkReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    switch(httpCode){
    case 200:
        msgBox.setIcon(QMessageBox::Information);
        msgBox.setText("Le SMS a été envoyé sur votre mobile.");
        break;
    case 400:
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("Echec de l'envoi :\nUn des paramètres obligatoires est manquant.");
        break;
    case 402:
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("Echec de l'envoi :\nTrop de SMS ont été envoyés en trop peu de temps.");
        break;
    case 403:
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("Echec de l'envoi :\nLe service n'est pas activé sur l'espace abonné, ou login / clé incorrect.");
        break;
    case 500:
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("Echec de l'envoi :\nErreur côté serveur. Veuillez réessayer ultérieurement.");
        break;
    default:
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText( "Erreur inconnue !\nCode d'erreur : "+QString::number(networkReply->error())+"\nRéponse HTTP : "+QString::number(httpCode));
        break;
    }

    msgBox.exec();
}

void MainWindow::loadSettings(){
    ui->userLineEdit->setText(settings->value("user").toString());
    ui->passLineEdit->setText(settings->value("pass").toString());
}

void MainWindow::saveSettings(){
    settings->setValue("user",ui->userLineEdit->text());
    settings->setValue("pass",ui->passLineEdit->text());
}

MainWindow::~MainWindow()
{
    saveSettings();
    delete ui;
    delete settings;
}
