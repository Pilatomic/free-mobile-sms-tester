#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QUrlQuery>
#include <QByteArray>
#include <QMessageBox>
#include <QSettings>
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void sendMessage(void);
    void finished(void);

private:
    void loadSettings(void);
    void saveSettings(void);

    Ui::MainWindow *ui;
    QNetworkReply* networkReply;
    QNetworkAccessManager networkAccessManager;
    QSettings *settings;
};

#endif // MAINWINDOW_H
